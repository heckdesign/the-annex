let hamburger;
hamburger = document.getElementById('hamburger');
let overlay;
overlay = document.getElementById('overlay');

// Code to toggle open class on mobile hamburger menu.
hamburger.addEventListener('click', (event) =>{
  document.body.classList.toggle('open');
  document.documentElement.classList.toggle('open');
});

overlay.addEventListener('click', (e)=>{
  document.body.classList.toggle('open');
});

//Close alert message on form submission
let close;
close = document.getElementById('close');
if(close){
  close.addEventListener('click', (event) =>{
      event.currentTarget.parentNode.remove();
  });
}

function handleIntersect(entries){
  // Assign correct animation class when item enters the viewport based on class element has.

  entries.forEach(function(entry) {
    if (entry.isIntersecting) {
      if (entry.target.classList.contains('fade')){
        entry.target.classList.add('animate__fadeIn','show');
      } else if (entry.target.classList.contains('appear-left')){
        entry.target.classList.add('animate__fadeInLeft','show');
      } else if (entry.target.classList.contains('fadeInUp')){
        entry.target.classList.add('animate__fadeInUp','show');
      } else if (entry.target.classList.contains('appear-right')){
        entry.target.classList.add('animate__fadeInRight','show');
      }
    } else {
    }
  });
}
  
let target;
let fade;
target = document.querySelectorAll('.appear-right, .appear-left, .fade, .fadeInUp');

window.addEventListener('load', (event) =>{
  target.forEach(function(t){
    CreateObserver(t);
  });

}, false)


// Intersection observer to trigger fade / slide animations for homepage elements
function CreateObserver(element){
  let observer;

  let options = {
    rootMargin: '0px',
    threshold: .25,
  }

  observer = new IntersectionObserver(handleIntersect, options);
  observer.observe(element);
}

