from .base import *

DEBUG = False
ALLOWED_HOSTS = ['centraliasquare.com','cs.centraliasquare.com','vintageeventvenues.com','centraliasquareannex.com'] 
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ga1-zm3v_#qfgyuf9-vy#&*&5hso^3*k50zc^+o%gt=+*bknhb'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_BACKEND = 'django_sendmail_backend.backends.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = '172.17.0.1'

RECAPTCHA_PUBLIC_KEY = '6LcPRAcaAAAAAKn6xFtGsYVYAdEv0oX5Y3nhKN1M'
RECAPTCHA_PRIVATE_KEY = '6LcPRAcaAAAAAD9Wg8vTeW_FqdW_DUVolmwsqOC1'
# EMAIL_PORT = 25
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = ''
# EMAIL_USE_TLS = False
# DEFAULT_FROM_EMAIL = 'Castleguard Contact <contact@castleguardsports.com>'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'smtp'
EMAIL_PORT = 25

try:
    from .local import *
except ImportError:
    pass
