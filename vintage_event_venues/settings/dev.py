from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ga1-zm3v_#qfgyuf9-vy#&*&5hso^3*k50zc^+o%gt=+*bknhb'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*'] 

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
RECAPTCHA_SITE_KEY = '6Le69wYaAAAAAIbuAFXcZBUFFZNzV13a4iWq24sr'
RECAPTCHA_SECRET_KEY = '6Le69wYaAAAAABjc6p6NBxgLAsUm06-NJt_RDy5Z'


try:
    from .local import *
except ImportError:
    pass
