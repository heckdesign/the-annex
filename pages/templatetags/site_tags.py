from django import template

from wagtail.core.models import Page, Site
from home.models import SocialMediaSetting
register = template.Library()

# @register.inclusion_tag(takes_context=True)
@register.simple_tag()
def cat_num(count):
    if count == 3:
        return 'column-33'

    elif count == 2:
        return 'column-50'
    elif count == 1:
        return 'column-100'


@register.simple_tag(takes_context=True)
def social_media(context):
    social_media =  SocialMediaSetting.for_site(context['current_site'])
    # print(social_media.social_media_items.all)
    # for s in social_media.social_media_items.all():
        # print(s)
    # print('Page: ', context['current_site'])
    return social_media.social_media_items.all()

@register.simple_tag(takes_context=True)
def get_site_root(context):
    #Get the root page of the site.
    return Site.find_for_request(context['request']).root_page

def has_menu_children(page):
    #Function the determines whether the current page has children
    return page.get_children().live().in_menu().exists()

def has_children(page, current_page):
    #get children of index pages
    return page.get_children().live().in_menu().exists()

def is_active(page, current_page):
    #So we can tell when a menu item is active
    return( current_page.url_path.startswith(page.url_path) if current_page else False)

# Retrieves the top menu items - the immediate children of the parent page
# The has_menu_children method is necessary because the Foundation menu requires
# a dropdown class to be applied to a parent
@register.inclusion_tag('tags/top_menu.html', takes_context=True)
def top_menu(context, parent, calling_page=None):
    menuitems = parent.get_children().live().in_menu()
    for menuitem in menuitems:
        menuitem.show_dropdown = has_menu_children(menuitem)
        # We don't directly check if calling_page is None since the template
        # engine can pass an empty string to calling_page
        # if the variable passed as calling_page does not exist.
        menuitem.active = (calling_page.url_path.startswith(menuitem.url_path)
                           if calling_page else False)
    return {
        'calling_page': calling_page,
        'menuitems': menuitems,
        # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }

# Retrieves the children of the top menu items for the drop downs
@register.inclusion_tag('tags/top_menu_children.html', takes_context=True)
def top_menu_children(context, parent, calling_page=None):
    menuitems_children = parent.get_children()
    menuitems_children = menuitems_children.live().in_menu()
    for menuitem in menuitems_children:
        menuitem.has_dropdown = has_menu_children(menuitem)
        # We don't directly check if calling_page is None since the template
        # engine can pass an empty string to calling_page
        # if the variable passed as calling_page does not exist.
        menuitem.active = (calling_page.url_path.startswith(menuitem.url_path)
                           if calling_page else False)
        menuitem.children = menuitem.get_children().live().in_menu()
    return {
        'parent': parent,
        'menuitems_children': menuitems_children,
        # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }