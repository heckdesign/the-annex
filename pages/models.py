from django.db import models

from modelcluster.fields import ParentalKey
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField, StreamField
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, InlinePanel, StreamFieldPanel, MultiFieldPanel
from django.shortcuts import render
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from .blocks import PricingStreamBlock, PricingTable, BaseStreamBlock
from wagtailcaptcha.models import WagtailCaptchaEmailForm
from django.conf import settings

# Create your models here.
class BasicPage(Page):
    hero = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')
    hero_heading = models.CharField(max_length=255, blank=True)
    hero_body = RichTextField(blank=True)
    body = StreamField(
        BaseStreamBlock(), blank=True
    )

    content_panels = Page.content_panels + [
        InlinePanel('hero_images', label='Hero Images'),
        MultiFieldPanel([
            FieldPanel('hero_heading'),
            FieldPanel('hero_body'),
        ]),
        ImageChooserPanel('hero'),
        StreamFieldPanel('body'),
    ]

#Hero image Orderable object for basic page
class HeroImage(Orderable):
    page = ParentalKey(BasicPage, on_delete=models.CASCADE, related_name='hero_images')
    image = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')

    panels = [
        ImageChooserPanel('image')
    ]


class RoomPage(BasicPage):
    square_footage = models.CharField(max_length=255, blank=True)
    capacity = models.CharField(max_length=255, blank=True)

    pricing = StreamField([
            ('pricing', PricingTable(required=False)),
    ],blank=True)

    content_panels = BasicPage.content_panels + [
        
        MultiFieldPanel(
            [
                FieldPanel('square_footage'),  
                FieldPanel('capacity'), 
            ],
            heading="Room Information"
        ),
        InlinePanel('amenities', label='Amenities'),
        StreamFieldPanel('pricing'),
        InlinePanel('photo_gallery', label='photo', heading='Photo Gallery'),
    ]

    # Parent page types
    parent_page_types = ['pages.RoomIndexPage']

class RoomIndexPage(BasicPage):
    # Index page for room objects. 
    # Queryset is configured to return only children of current indexpage that are currently live.
    content_panels = BasicPage.content_panels + [
        
    ]

    def get_context(self, request):
        context = super().get_context(request)
        rooms = self.get_children().specific().live()
        context['rooms'] = rooms
        return context


class Amenity(Orderable):
    #Amenity object for room pages. 
    page = ParentalKey(RoomPage, on_delete=models.CASCADE, related_name='amenities')
    title = models.CharField(max_length=255)
    description = RichTextField(blank=True)

    panels = [
        FieldPanel('title'),
        FieldPanel('description'),
    ]

class PhotoGallery(Orderable):
    #Orderable items for BasicPage photogallery.
    page = ParentalKey(BasicPage, on_delete=models.CASCADE, related_name='photo_gallery')
    image = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')

    panels = [
        ImageChooserPanel('image'),
    ]

class FormField(AbstractFormField):
    page = ParentalKey('FormPage', on_delete=models.CASCADE, related_name='form_fields')

class FormPage(WagtailCaptchaEmailForm):
    image = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')
    phone = models.CharField(max_length=255, blank=True)
    address_line_one = models.CharField(max_length=255, blank=True)
    address_line_two = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)
     
    content_panels = AbstractEmailForm.content_panels + [
        ImageChooserPanel('image'),
        MultiFieldPanel([
            FieldPanel('phone'),
            FieldPanel('address_line_one'),
            FieldPanel('address_line_two'),
            FieldPanel('email'),
        ], 'Contact Information'),
        FieldPanel('intro', classname="full"),
        InlinePanel('form_fields', label="Form fields"),
        FieldPanel('thank_you_text', classname="full"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    def serve(self, request, *args, **kwargs):
        # Override the serve method and return a success message to the form page.
        # If request is POST and form is valid process the contact form and return success message.
        # Otherwise render the form
        if request.method == 'POST': 
             form = self.get_form(request.POST, page=self, user=request.user)

             if form.is_valid():
                 self.process_form_submission(form)
                 context = self.get_context(request)
                 form = self.get_form(page=self)
                 context['message'] = 'Success. Your message has been sent.'
                 context['form'] = form
                 return render (
                     request,
                     self.get_template(request),
                     context
                 )

        else:
            form = self.get_form(page=self)

        context = self.get_context(request)
        context ['form'] = form
        return render(
            request,
            self.get_template(request),
            context
        )