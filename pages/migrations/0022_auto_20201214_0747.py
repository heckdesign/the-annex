# Generated by Django 3.1 on 2020-12-14 07:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0021_auto_20201214_0737'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formpage',
            name='address',
        ),
        migrations.AddField(
            model_name='formpage',
            name='address_line_one',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name='formpage',
            name='address_line_two',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
