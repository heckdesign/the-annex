# Generated by Django 3.1 on 2020-12-07 07:24

from django.db import migrations
import pages.blocks
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0015_auto_20201207_0549'),
    ]

    operations = [
        migrations.AddField(
            model_name='roompage',
            name='pricing',
            field=wagtail.core.fields.StreamField([('pricing', wagtail.core.blocks.ListBlock(pages.blocks.PricingTable))], blank=True, verbose_name='Pricing'),
        ),
    ]
