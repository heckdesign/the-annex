from wagtail.images.blocks import ImageChooserBlock
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.core.blocks import (
    CharBlock, ChoiceBlock, BooleanBlock, RichTextBlock, StreamBlock, StructBlock, StructValue, TextBlock, ListBlock, URLBlock, PageChooserBlock
)

#Pricing Table
class PricingTable(StructBlock):
    # heading = CharBlock(required=True)
    
    pricing_table = TableBlock(required=True)
    body = RichTextBlock(required=False)

    class Meta:
        template = "blocks/pricing_table.html"

class LinkStructValue(StructValue):
    def url(self):
        external_url = self.get('external_url')
        page = self.get('page')
        document = self.get('document')
        if external_url:
            return external_url
        elif page:
            return page.url
        elif document:
            return document.url

class ButtonBlock(StructBlock):
    text = CharBlock(label='Link Text', required=True)
    page = PageChooserBlock(label='page', required=False)
    external_url = URLBlock(label='external url', required=False)
    document = DocumentChooserBlock(label='document', required=False)
    reverse_style = BooleanBlock(required=False)

    class Meta:
        icon = 'site'
        template = 'blocks/button_block.html'
        value_class = LinkStructValue

# class PricingStreamBlock(StreamBlock):
#     pricing = PricingTable()
#     pricing = PricingTable()
#Table Block
# class TableBlock(TableBlock):
# class PricingBlock(StructBlock):
#     title = CharBlock(required=True)
#     square_footage = CharBlock(required=True)
#     image = ImageChooserBlock()
#     pricing = ListBlock(PricingTable)

class PricingStreamBlock(StreamBlock):
    pricing = PricingTable(required=False)

class BaseStreamBlock(StreamBlock):
    paragraph = RichTextBlock(required=False)
    button = ButtonBlock(required=False)
    pricing = PricingTable(required=False)



    