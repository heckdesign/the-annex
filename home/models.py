from django.db import models

from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalKey
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from wagtail.contrib.settings.models import BaseSetting, register_setting
from pages.blocks import ButtonBlock

# Orderable wagtail setting to administer social media items
@register_setting
class SocialMediaSetting(ClusterableModel, BaseSetting):
    panels = [
        InlinePanel('social_media_items', label='Social Media Items'),
    ]

#Social media items will appear in the header and footer.
class SocialMediaItem(Orderable):
    url = models.URLField()
    icon = models.CharField(max_length=255)
    media_setting = ParentalKey(SocialMediaSetting, related_name='social_media_items')

    panels = [
        FieldPanel('url'),
        FieldPanel('icon')
    ]

# QuickLink block with fields for internal and external links
class QuickLinkBlock(blocks.StructBlock):
    text = blocks.CharBlock(label="link text", required=True)
    page = blocks.PageChooserBlock(label="page", required=False)
    external_url = blocks.URLBlock(label="external URL", required=False)

    class Meta:
        icon = 'site'


class CategoryBlock(blocks.StructBlock):
    image = ImageChooserBlock()
    link = QuickLinkBlock()

    def get_context(self, value, parent_context=None):
        # Override the default context
        # If link type is internal return first, then external, and finally empty string if no link exists.
        context = super().get_context(value, parent_context=parent_context)
        if value['link']['page']:
            context['url'] = value['link']['page'].url
        elif value['link']['external_url']:
            context['url'] = value['link']['external_url']
        else:
            context['url'] = ''
        return context

    def get_template(self, context):
        # return specific template for list_block if site id is 3. 
        # This is not ideal as the site id could change if a site is deleted and readded.
        if context['current_site'].id == 3:
            return 'blocks/centralia_square/category_list_block.html'
        else:
            return 'blocks/category_list_block.html'  

    class Meta:
        template = 'blocks/category_list_block.html'  

class CategoryStreamBlock(blocks.StreamBlock):
    category = CategoryBlock()

    class Meta:
        template = 'blocks/category_stream_block'

# Collection of category blocks with a few fields to customize each block.
class CategoriesBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    icon = ImageChooserBlock(required=False)
    body = blocks.RichTextBlock(required=False)
    category_list = CategoryStreamBlock()


    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context 

    class Meta:
        icon = 'pilcrow'
        template = 'blocks/categories_block.html' 

#Room block for homepage display.
class RoomBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    body = blocks.RichTextBlock()
    icon = ImageChooserBlock(required=False)
    image = ImageChooserBlock(required=False)
    link = QuickLinkBlock()

    def get_template(self, context):
        if context['current_site'].id == 3:
            return 'blocks/centralia_square/room_block.html'
        else:
            return 'blocks/room_block.html'

    class Meta:
        icon = 'user'
        template = 'blocks/room_block.html'

#Streamblock of orderable roomblocks
class RoomStreamBlock(blocks.StreamBlock):
    rooms = RoomBlock()

    class Meta:
        template = 'blocks/rooms_stream_block.html'

# Home page object that has Hero section with overlays, body streamfield containing room and category streamfields.
class HomePage(Page):
    hero_heading = models.CharField(max_length=255, blank=True)
    hero_body = RichTextField(blank=True)
    heading = models.CharField(max_length=255, blank=True)
    sub_heading = models.CharField(max_length=255, blank=True)
    intro = RichTextField(blank=True)

    button = StreamField(
        blocks.StreamBlock(
            [("button", ButtonBlock())],
            max_num=1, required=False,
        ),
        blank=True
    )

    body = StreamField ([
        ('rooms', RoomStreamBlock(required=False)),
        ('categories', CategoriesBlock()),
    ], blank=True)

    search_fields = Page.search_fields +[
        index.SearchField('intro')
    ]

    content_panels = Page.content_panels + [
        InlinePanel('hero_images', label='Hero Images'),
        MultiFieldPanel([
            FieldPanel('hero_heading'),
            FieldPanel('hero_body'),
        ]),
        InlinePanel('promotions', label='Promotion', heading='Promotions'),
        MultiFieldPanel([
            FieldPanel('heading'),
            FieldPanel('sub_heading'),
            FieldPanel('intro'),
            StreamFieldPanel('button'),
        ],
        heading="Call to action",
        ),
        StreamFieldPanel('body'),

    ]


class LandingPage(Page):
    body = RichTextField(blank=True)

    content_panels =  Page.content_panels + [
        FieldPanel('body'),
        InlinePanel('site_items', label='Site Item'),
    ]

#Site item orderable for Landing Pages
class SiteItem(Orderable):
    page = ParentalKey(LandingPage, on_delete=models.CASCADE, related_name='site_items')
    title = models.CharField(max_length=250)
    image = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')
    
    link = models.URLField('Link', blank=True)

    panels = [
        FieldPanel('title'),
        ImageChooserPanel('image'),
        FieldPanel('link'),
    ]

# Orderable hero images for the homepage slider
class HeroImage(Orderable):
    page = ParentalKey(HomePage, on_delete=models.CASCADE, related_name='hero_images')
    image = models.ForeignKey('wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+')

    panels = [
        ImageChooserPanel('image')
    ]

#Promotion items for the HomePage. 
class Promotion(Orderable):
    page = ParentalKey(HomePage, on_delete=models.CASCADE, related_name='promotions')
    title = models.CharField(max_length=255)
    body = RichTextField(blank=True)
    link = StreamField([
        ('link', QuickLinkBlock(required=False, max_num=1))
    ])

    panels = [
        FieldPanel('title'),
        FieldPanel('body'),
        StreamFieldPanel('link'),
    ]  

