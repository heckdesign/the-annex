# Generated by Django 3.1 on 2020-11-02 06:32

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_auto_20201023_0555'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='rooms',
            field=wagtail.core.fields.StreamField([('rooms', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('body', wagtail.core.blocks.RichTextBlock()), ('icon', wagtail.images.blocks.ImageChooserBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False))], required=False))], blank=True),
        ),
    ]
