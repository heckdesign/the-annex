# Generated by Django 3.1 on 2020-11-28 07:18

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0014_auto_20201127_0701'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.core.fields.StreamField([('rooms', wagtail.core.blocks.StreamBlock([('rooms', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('body', wagtail.core.blocks.RichTextBlock()), ('icon', wagtail.images.blocks.ImageChooserBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False)), ('link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(label='link text', required=True)), ('page', wagtail.core.blocks.PageChooserBlock(label='page', required=False)), ('external_url', wagtail.core.blocks.URLBlock(label='external URL', required=False))]))]))], required=False)), ('categories', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('icon', wagtail.images.blocks.ImageChooserBlock(required=False)), ('body', wagtail.core.blocks.RichTextBlock(required=False)), ('category_list', wagtail.core.blocks.StreamBlock([('category', wagtail.core.blocks.StructBlock([('image', wagtail.images.blocks.ImageChooserBlock()), ('link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(label='link text', required=True)), ('page', wagtail.core.blocks.PageChooserBlock(label='page', required=False)), ('external_url', wagtail.core.blocks.URLBlock(label='external URL', required=False))]))]))]))]))], blank=True),
        ),
    ]
