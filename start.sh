 #!/bin/bash
 docker-compose -f docker-compose.yml -f production.yml run --rm app python manage.py compress --force
 docker-compose -f docker-compose.yml -f production.yml run --rm app python manage.py collectstatic --no-input
 docker-compose -f docker-compose.yml -f production.yml run --rm app python manage.py migrate --no-input
 docker-compose -f docker-compose.yml -f production.yml up -d